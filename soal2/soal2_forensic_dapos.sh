#!/bin/bash

# Rata-rata serangan
awk -F ":" '
{
  if (NR!=1) {
    count[$3]++;
    n++;
  }
} 
END { 
  avg = n/length(count);
  printf("Rata-rata serangan adalah sebanyak %f requests per jam\n", avg);
}
' log_website_daffainfo.log > ratarata.txt


# IP terbanyak req server
awk -F ":" '
{
  if (NR!=1) {
    count[$1]++;
    n++;
  }
} 
END {
  max = 0;
  maxIP = "";

  for (IP in count) {
    if (count[IP] > max) {
      max = count[IP];
      maxIP = IP;
    }
  };

  printf("IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", maxIP, max);
}
' log_website_daffainfo.log > result.txt


# User-agent curl
awk -F ":" '
BEGIN {
  n = 0;
}
$NF ~ /curl/ {
  n++;
}
END {
  printf("\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", n);
} 
' log_website_daffainfo.log >> result.txt


# Serangan jam 2 tgl 22
awk -F ":" '
$3 == 02 {
  print $1;
}
' log_website_daffainfo.log >> result.txt
