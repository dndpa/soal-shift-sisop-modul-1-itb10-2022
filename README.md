# soal-shift-sisop-modul-1-ITB10-2022

# Soal 1 (rev)
###### by Adinda
## Soal

Bantulah Han untuk membuat program dengan detail ketentuan:

a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh

b. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :

- dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
- att:
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

## Penyelesaian

#### - FILE `register.sh` (info Registrasi disimpan dalam file log.txt dan data Registrasi disimpan dalam file user.txt)
Pertama, kita memprint Greeting Text sebagai opening dari script tersebut. Lalu meminta user untuk menginputkan Username yang akan didaftarkan. Tanggal dan jam detail saat user registrasi akan langsung tercatat dalam fungsi `timestamp()` sebagai berikut:
```bash
#!/bin/bash/

# Greeting Text
echo "WELCOME GUEST!!"
echo -e "Please Register Yourself :)\n"

read -p "Input your Username: " uname

# Fungsi untuk print format detail tgl & jam
timestamp() {
	date +'%m/%d/%Y %H:%M:%S'
}
now=$(timestamp)
```

Lalu dibuat fungsi `cekuname()` untuk mengecek apakah uname yang diinputkan user sudah terdaftar di `user.txt` atau belum dengan menggunakan grep seperti berikut:
```bash
# Cek apakah uname sudah terdaftar apa belum di user.txt menggunakan grep
cekuname() {
	until ! grep -q "$uname" ./users/user.txt
	do
		echo -e "$now REGISTER: ERROR User already exist" >> log.txt
		read -p "User already exists. Input another Username: " uname
	done
}
cekuname #panggil fungsi cekuname
```

Setelah itu, dilakukan pengecekan Password sesuai dengan ketentuan dari soal yaitu minimal berisi 8 karakter, memiliki minimal 1 huruf kapital dan 1 huruf kecil, alphanumeric, dan tidak boleh sama dengan username. Berikut merupakan scriptnya:
```bash
# Cek apakah password sudah sesuai dengan aturan pada soal
read -s -p "Enter Password : " pwd
while [[ `expr length "$pwd"` -lt 8 || "$pwd" =~ [^0-9a-zA-Z] || ( ! "$pwd" =~ [[:upper:]]) || ( ! "$pwd" =~ [[:lower:]] ) || "$pwd" == "$uname"  ]]
do
	echo -e "\n"
	#minimal 8 karakter
	if [ `expr length "$pwd"` -lt 8 ]
	then
		echo "Password should consist minimum 8 characters"
	fi
	
	#minimal 1 uppercase & 1 lowercase & alphanumeric
	if [[ "$pwd" == *[A-Z]* && "$pwd" == *[a-z]* && "$pwd" == *[0-9]* && ( ! "$pwd" =~ [[:upper:]]) && ( ! "$pwd" =~ [[:lower:]]) ]]
	then
		echo "Password should consist minimum 1 uppercase & 1 lowercase & alphanumeric"
	fi
	
	#pass tidak boleh sama dengan uname
	if [ "$pwd" == "$uname" ]
	then
		echo "Password should different with Username"
	fi
	
	read -s -p "Enter again pass: " pwd
done
```
```bash
echo $uname $pwd >> ./users/user.txt

echo "$now REGISTER: INFO User $uname registered succesfully" >> log.txt
echo -e "\nRegister Success Maazzehh!"
```
Setelah user menginputkan password sesuai dengan ketentuan, maka data Username dan Password akan disimpan dalam file `user.txt` beserta info waktu registernya akan disimpan dalam file `log.txt`.

Berikut merupakan isi file `user.txt` berupa data Username dan Password:
![image.3.png](./image.3.png)

Berikut merupakan isi file `log.txt` berupa info detail REGISTER dan juga LOGIN:
![image.4.png](./image.4.png)


#### - FILE `main.sh` (untuk login dan info Login disimpan dalam file log.txt)
Pada file ini, user yang sudah registrasi dan terdata bisa melakukan login.
Apabila username & password yang diinputkan salah, maka akan tercatat info LOGIN: ERROR pada file `log.txt`. Namun, apabila username & password yang diinputkan sesuai dengan data pada `user.txt`, maka akan tercatat user logged in.
```bash
#! /bin/bash/

# Greeting Text
echo "WELCOME GUEST!!"
echo -e "Please Login Yourself :)\n"

read -p "Input Username: " uname
read -s -p "Input Password: " pwd

timestamp() {
        date +'%m/%d/%Y %H:%M:%S'
}
now=$(timestamp)

true=$(awk /^$uname.*$pwd/' {print "1"}' ./users/user.txt)

if [[ "$true" == 1 ]];
then
	echo -e "$now LOGIN: INFO User $uname logged in" >> log.txt
	echo -e "You succesfully login!"
else
	echo -e "\nUsername atau Password salah mazzehh"
	echo "${now} LOGIN: ERROR Failed login attempt on user $uname" >> log.txt
fi
```
Lalu user akan diminta untuk memasukkan suatu command antara `dl N` atau `att` seperti berikut:
```bash
read -p "Please input command (dl N / att) : " comm mand
	if [[ "$comm" == "att" ]]
	then
		awk '
		BEGIN { }
		/LOGIN/ {++n}
		END { print n }' log.txt
	else
		noww=$(date +'%Y-%m-%d')
		if [ -e "${noww}_$uname".zip ]
		then
			unzip "${noww}_$uname".zip
		else
			mkdir "${noww}_$uname"
		fi
		cd "${noww}_$uname"/
		
		for ((a=1; a<=mand; a=a+1))
		do
			if [[ "$a" -lt 10 ]]
			then
				wget --output-document=PIC_0"$a" https://loremflickr.com/320/240
			else
				wget --output-document=PIC"$a" https://loremflickr.com/320/240
			fi
		done
		cd ..
		zip -P "$pwd" -r "${mand}_$uname.zip" "${mand}_$uname"
	fi
```
- Apabila user menginputkan `att`, maka terminal akan mengoutputkan  jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

![image.5.png](./image.5.png)

- Apabila user menginputkan `dl N` ( N = Jumlah gambar yang akan didownload), maka akan mendownload gambar dari https://loremflickr.com/320/240 sesuai dengan banyaknya N dan dimasukkan ke dalam folder sesuai dengan format yang diminta oleh soal seperti berikut ini:

![image.6.png](./image.6.png)

## Kendala
- Kesulitan saat menjalankan logic dari program `register.sh` karena password yang diinputkan sempat error dan kurang sesuai dengan ketentuan
- Kesulitan pada nomor 3d karena sempat tidak paham dengan maksud soal, setelah paham pun masih kesulitan dalam membuat logic command yang diminta sesuai ketentuan soal



# Soal 2
###### by Salman

## Soal

Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:
- Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
- Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
- Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
- Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
- Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.


Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:
- File ratarata.txt : Rata-rata serangan adalah sebanyak rata_rata requests per jam
- File result.txt : 
IP yang paling banyak mengakses server adalah ip_address sebanyak jumlah_request requests

- Ada jumlah_req_curl requests yang menggunakan curl sebagai user-agent

IP Address Jam 2 pagi, 
IP Address Jam 2 pagi, 
dst
 * Gunakan AWK** Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder forensic_log_website_daffainfo_log


## Penyelesaian

Pada bagian pertama kita hanya diperintahkan untuk membuat folde soalnya yang bernama forensic_log_website_daffainfo_log. Kita hanya perlu menggunakan command mkdir saja saja seperti

```bash
mkdir "forensic_log_website_daffainfo_log"
```

selanjutnya kita diperintahkan mencari rata-rata serangan request perjam kita dapat mencarinya dengan menggunakan AWK dengan pemisah kolom “:” dan kita buat statement NR!=1 agar kolom pertama yang berupa judul tidak dibaca.

```bash
"IP":"Date":"Request":"Status Code":"Content Length":"User Agent"
```

setelah itu kita buat variable n untuk jumlahnya yang setiap loop bertambah count{$3}++ untuk menghitung panjangnyaa lalu kita bagi saja n/length(count) untuk rata-raat serangan. Dan jangan lupa dimasukkan ke file ratarata.txt dengan command > agar outputnya dikirim ke file tersebut.

```bash
# Rata-rata serangan
awk -F ":" '
{
  if (NR!=1) {
    count[$3]++;
    n++;
  }
} 
END { 
  avg = n/length(count);
  printf("Rata-rata serangan adalah sebanyak %f requests per jam\n", avg);
}
' log_website_daffainfo.log > ratarata.txt
```

Selanjutnya kita diperintahkan untuk mencari IP yang paling banyak menyerang dan jumlah total menyerangnya. Dengan menggunakan command awk sama seperti di atas bedanya $3 nya diganti $1 karena IP berada di kolom pertama.

```bash
"IP":"Date":"Request":"Status Code":"Content Length":"User Agent"
"45.146.165.37":"22/Jan/2022:00:11:10":"POST /cgi-bin/.%2e/.%2e/.%2e/.%2e/bin/sh HTTP/1.1":400:5633:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
```
Lalu kita buat loop nama ip sebagai count dan buat permisalan count[IP] > max maka max sama dengan count[IP] yaitu jumlah ip menyerang websitenya dan maxIP sebagai IP yang paling banyak menyerang lalu hasilnya dikirim ke result.txt logikanya sama seperti di atas.

```shell
# IP terbanyak req server
awk -F ":" '
{
  if (NR!=1) {
    count[$1]++;
    n++;
  }
} 
END {
  max = 0;
  maxIP = "";

  for (IP in count) {
    if (count[IP] > max) {
      max = count[IP];
      maxIP = IP;
    }
  };

  printf("IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", maxIP, max);
}
' log_website_daffainfo.log > result.txt
```

Selanjutnya kita menggunakan awk lagi dan kita cari saja jika disetiap baris ada tulisan curl maka variable n =0 jadi bertambah sesuai jumlah baris yang ada curlnya  lalu outputnya di kirim ke result.txt juga dengan comman “>>” agar filenya tidak ketiban yang sebelumnya. Jadi hasilnya ditaruh dibawah hasilnya.

```shell
# User-agent curl
awk -F ":" '
BEGIN {
  n = 0;
}
$NF ~ /curl/ {
  n++;
}
END {
  printf("\nAda %d requests yang menggunakan curl sebagai user-agent\n\n", n);
} 
' log_website_daffainfo.log >> result.txt
```

Terakhir kita diperintahkan untuk mencari tahu siapa IP yang melakukan serangan pada jam 2. Disini kita menggunakan awk lagi dengan memisalkan kolom 3 =  02 karena kolom ke-3 sama dengan jam.

```bash
"IP":"Date":"Request":"Status Code":"Content Length":"User Agent"
"45.146.165.37":"22/Jan/2022:00:11:10":"POST /cgi-bin/.%2e/.%2e/.%2e/.%2e/bin/sh HTTP/1.1":400:5633:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
```

Lalu kita print siapa yang melakukan serangan dengan print kolom 1 lalu hasilnya lalu dikirim ke result.txt.

```shell
# Serangan jam 2 tgl 22
awk -F ":" '
$3 == 02 {
  print $1;
}
' log_website_daffainfo.log >> result.txt
```

## Kendala
- Sempat bingung karena saya kira harus menghubungkan dengan website ternyata tidak karena sudah diberi file log-nya.



# Soal 3 
###### by Adinda, Salman & Dhika

## Soal

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

- Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
- Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
- Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
- Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Note:
- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62


## Penyelesaian

#### a. Mencatat semua metrics yang berisikan data monitoring ram & size ke dalam suatu file log 

Pertama, kita membuat 2 variabel yaitu variabel now dan log_file.
- Variabel now digunakan untuk menampilkan detail tanggal dan jam  
- Variabel log_file digunakan untuk membuat file di dalam HOME dan di folder log, yang mana nama file yang akan dibuat sesuai dengan format yang diminta pada soal (contoh: metrics_202201311150000.log)
```bash
#!/bin/bash

# The name of log file.
now=`date +%Y%m%d%H%M%S`
log_file="$HOME/log/metrics_$now.log"
```

Lalu kita membuat fungsi utama logging() yang berisikan fungsi header dan information seperti berikut:
```bash
# The main function for log.
logging() {
  header
  information
}
```

Fungsi header digunakan untuk memprint output yang diminta pada soal seperti mem_total hingga path_size seperti berikut:
```bash
# Create the header of log file.
header() {
  echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
}
```

Fungsi information digunakan untuk memprint penggunaan ram dan size dari path suatu directory.
- `free -m` digunakan untuk menyatakan penggunaan ram
- `du -sh $path` digunakan untuk menyatakan penggunaan size suatu directory yang mana target path yang dimonitor tersebut $HOME
Codenya ialah sebagai berikut:
```bash
# Create the information of log file.
information() {
  mem_total=`free -m | grep Mem | awk '{print $2}'`
  mem_used=`free -m | grep Mem | awk '{print $3}'`
  mem_free=`free -m | grep Mem | awk '{print $4}'`
  mem_shared=`free -m | grep Mem | awk '{print $5}'`
  mem_buff=`free -m | grep Mem | awk '{print $6}'`
  mem_available=`free -m | grep Mem | awk '{print $7}'`
  swap_total=`free -m | grep Swap | awk '{print $2}'`
  swap_used=`free -m | grep Swap | awk '{print $3}'`
  swap_free=`free -m | grep Swap | awk '{print $4}'`
  path=$HOME
  path_size=`du -sh $path | awk '{print $1}'`
  echo "$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$path,$path_size"
}
```

Jika kita mengetikkan `free -m` pada terminal, maka tampilan yang keluar akan seperti berikut:
![image.1.png](./image.1.png)

Maka dari itu, kita menggunakan `grep` untuk mengambil suatu baris yang berisikan keywoard "Mem" atau "Swap" (menyesuaikan dengan variabel yang diminta. 
Lalu kita gunakan awk untuk memprint size dimana variabel tersebut berada. (Misal: untuk memprint size dari mem_free, kita gunakan `grep Mem` untuk mencari keyword "Mem" dan menggunakan `awk '{print $4}'` untuk memprint kata ke-4 di baris tersebut.)


Setelah itu, kita memprint semua isi fungsi logging di atas dan menaruh semua outputnya ke dalam setiap file metrics log_file. 
``` bash
# Output the log file.
logging > $log_file
```


#### b. Membuat agar script untuk mencatat metrics di atas dapat berjalan otomatis pada setiap menit

Untuk menjalankan script tersebut setiap menit, dapat dengan cara mengetikkan `crontab -e` pada terminal lalu mengetikkan seperti berikut:
```shell
* * * * * bash $HOME/soal-shift-sisop-modul-1-ITB10-2022/soal3/minute_log.sh
```
Script pun akan dijalankan setiap menit seperti berikut:
![image.2.png](./image.2.png)


#### c. Membuat file agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis yang berisikan nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics

script aggregate_minutes_to_hourly_log.sh dijalankan setiap satu jam untuk mendata log file yang dibuat oleh minute_log.sh satu jam sebelum script aggregate_minutes_to_hourly_log.sh dijalankan.

Script di bawah menginisialisasi variabel penting yaitu nama direktori penyimpanan log, kemudian array monthDay untuk mempermudah perhitungan mundur 1 jam sebelumnya bila membuat mundur bulan.

```shell

	now=`date +%Y%m%d%H%M%S`
	ago=`date +%Y%m%d%H%M%S -d  "1 hour ago 10 seconds ago"`

	file_name="./log/metrics_$time.log"

```

untuk mendapatkan metics maksimum, metrics minimum dan rata-rata metrics dari file kita membutuhkan script sebagai berikut :

```shell

	mem_total=`cat $file_name | awk -F ',' '{if (NR!=1) {print $1}}'` 
    mem_used=`cat $file_name | awk -F ',' '{if (NR!=1) {print $2}}'`
    mem_free=`cat $file_name | awk -F ',' '{if (NR!=1) {print $3}}'`
    mem_shared=`cat $file_name | awk -F ',' '{if (NR!=1) {print $4}}'`
    mem_buff=`cat $file_name | awk -F ',' '{if (NR!=1) {print $5}}'`
    mem_available=`cat $file_name | awk -F ',' '{if (NR!=1) {print $6}}'`
    swap_total=`cat $file_name | awk -F ',' '{if (NR!=1) {print $7}}'`
    swap_used=`cat $file_name | awk -F ',' '{if (NR!=1) {print $8}}'`
    swap_free=`cat $file_name | awk -F ',' '{if (NR!=1) {print $9}}'`
    path=`cat $file_name | awk -F ',' '{if (NR!=1) {print $10}}'`
    path_size=`cat $file_name | awk -F ',' '{if (NR!=1) {print $11}}'`


```

dibawah ini adalah script untuk log. tepatnya adalah list file log dan command log untuk setiap file

```shell

for time in $(echo $var); do
  if [ $time -ge $ago ] && [ $time -le $now ]; then

```


## Kendala
- Sempat bingung saat mengambil suatu nomor dalam tabel free -m, namun setelah diulik lagi ternyata solusinya menggunakan grep dan awk
- Kesulitan dalam membuat file agregasi karena harus mengambil dan membandingkan cukup banyak data
