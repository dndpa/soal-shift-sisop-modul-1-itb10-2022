#!/bin/bash

# The name of log file.
now=`date +%Y%m%d%H%M%S`
log_file="$HOME/log/metrics_$now.log"

# The main function for log.
logging() {
  header
  information
}

# Create the header of log file.
header() {
  echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
}

# Create the information of log file.
information() {
  mem_total=`free -m | grep Mem | awk '{print $2}'`
  mem_used=`free -m | grep Mem | awk '{print $3}'`
  mem_free=`free -m | grep Mem | awk '{print $4}'`
  mem_shared=`free -m | grep Mem | awk '{print $5}'`
  mem_buff=`free -m | grep Mem | awk '{print $6}'`
  mem_available=`free -m | grep Mem | awk '{print $7}'`
  swap_total=`free -m | grep Swap | awk '{print $2}'`
  swap_used=`free -m | grep Swap | awk '{print $3}'`
  swap_free=`free -m | grep Swap | awk '{print $4}'`
  path=$HOME
  path_size=`du -sh $path | awk '{print $1}'`
  echo "$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,$path,$path_size"
}

# Output the log file.
logging > $log_file
