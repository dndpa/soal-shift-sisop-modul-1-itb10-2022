# The time now and 1 hour ago with additional 10 seconds.
now=`date +%Y%m%d%H%M%S`
ago=`date +%Y%m%d%H%M%S -d  "1 hour ago 10 seconds ago"`

# List of log files.
var=`ls ./log | awk -F '[_.]' '{ print $2 }'`

# Initialize the variable.
max_mem_total=0
max_mem_used=0
max_mem_free=0
max_mem_shared=0
max_mem_buff=0
max_mem_available=0
max_swap_total=0
max_swap_used=0
max_swap_free=0
max_path_size=0

# For each log files.
for time in $(echo $var); do
  if [ $time -ge $ago ] && [ $time -le $now ]; then
    # The file name.
    file_name="./log/metrics_$time.log"
    # Get the metrics from the file.
    mem_total=`cat $file_name | awk -F ',' '{if (NR!=1) {print $1}}'` 
    mem_used=`cat $file_name | awk -F ',' '{if (NR!=1) {print $2}}'`
    mem_free=`cat $file_name | awk -F ',' '{if (NR!=1) {print $3}}'`
    mem_shared=`cat $file_name | awk -F ',' '{if (NR!=1) {print $4}}'`
    mem_buff=`cat $file_name | awk -F ',' '{if (NR!=1) {print $5}}'`
    mem_available=`cat $file_name | awk -F ',' '{if (NR!=1) {print $6}}'`
    swap_total=`cat $file_name | awk -F ',' '{if (NR!=1) {print $7}}'`
    swap_used=`cat $file_name | awk -F ',' '{if (NR!=1) {print $8}}'`
    swap_free=`cat $file_name | awk -F ',' '{if (NR!=1) {print $9}}'`
    path=`cat $file_name | awk -F ',' '{if (NR!=1) {print $10}}'`
    path_size=`cat $file_name | awk -F ',' '{if (NR!=1) {print $11}}'`

    # Get the max metrics.
    
    # Get the min metrics
  
    # Get the avg metrics

  fi
done


# Output the results.
